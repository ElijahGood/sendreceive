package arguments

import (
	"errors"
	"strconv"
)

var MAX_SEND = 600000
var MAX_RECEIVE = 600000

func ArgsHandler(args []string) (int, int, error) {
	send := 0
	receive := 0
	if len(args) == 2 || len(args) == 4 {

		if len(args) == 2 {
			new_send, err1 := strconv.Atoi(args[0])
			new_receive, err2 := strconv.Atoi(args[1])
			send = new_send
			receive = new_receive
			if err1 != nil || err2 != nil {
				return 0, 0, errors.New("Arguments should be numeric. Please, try: 'go run main.go 10 20' or 'go run main.go -f 10 -c 20' .")
			}
		} else {
			for i := 0; i < (len(args) - 1); i++ {
				if args[i] == "-f" || args[i] == "-c" {
					i++
					value, err := strconv.Atoi(args[i])
					if err != nil {
						return 0, 0, errors.New("Invalid arguments. Please, try: 'go run main.go 10 20' or 'go run main.go -f 10 -c 20' .")
					} else if args[i-1] == "-f" {
						send = value
					} else if args[i-1] == "-c" {
						receive = value
					}
				} else {
					return 0, 0, errors.New("Invalid arguments. Please, try: 'go run main.go 10 20' or 'go run main.go -f 10 -c 20' .")
				}
			}
		}

		if receive > MAX_RECEIVE && send > MAX_SEND {
			return 0, 0, errors.New("Too big argument. Limit is " + strconv.Itoa(MAX_RECEIVE))
		} else if receive <= 0 || send <= 0 {
			return 0, 0, errors.New("Invalid argumets. All arguments must be bigget than 0")
		} else {
			return send, receive, nil
		}

	} else {
		return 0, 0, errors.New("Invalid number of arguments. Please, try: 'go run main.go 10 20' or 'go run main.go -f 10 -c 20' .")
	}
}
