package main

import (
	"fmt"
	"os"
	"sync"

	"gitlab.com/sendrecieve/arguments"
	"gitlab.com/sendrecieve/receiver"
	"gitlab.com/sendrecieve/sender"
	"gitlab.com/sendrecieve/types"
)

func main() {
	send, receive, err := arguments.ArgsHandler(os.Args[1:])
	if err != nil {
		fmt.Println(string("\033[31m"), err)
		return
	}
	di := &types.DI{Send: send, Receive: receive}
	var wg sync.WaitGroup
	wg.Add(2)
	go receiver.RunReceiver(di, &wg)
	ch := make(chan types.JsonForm)
	go sender.Generator(di.Send, ch)
	go sender.Sender(di, &wg, ch)
	wg.Wait()
}
