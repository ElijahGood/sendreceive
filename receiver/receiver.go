package receiver

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"sync"
	"time"

	"gitlab.com/sendrecieve/types"
	"golang.org/x/time/rate"
)

type ReceiverHandler struct {
	Limiter *rate.Limiter
}

func Receiver(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/" {
		http.Error(w, http.StatusText(404), http.StatusNotFound)
		return
	} else if r.Method != "POST" {
		log.Println("Sorry, only POST method is allowed.")
		return
	}

	decoder := json.NewDecoder(r.Body)
	var t types.JsonForm
	err := decoder.Decode(&t)
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("	received[%d]: 	%s;\n", t.UserID, t.Message)
}

func (rh *ReceiverHandler) rateLimit(h http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		if rh.Limiter.Allow() == false {
			http.Error(w, http.StatusText(429), http.StatusTooManyRequests)
			return
		}

		h.ServeHTTP(w, r)
	}
}

func RunReceiver(di *types.DI, wg *sync.WaitGroup) {

	duration := (time.Minute / time.Duration(di.Receive))
	handler := &ReceiverHandler{
		Limiter: rate.NewLimiter(rate.Every(duration), 1),
	}

	mux := http.NewServeMux()
	mux.Handle("/", handler.rateLimit(Receiver))
	fmt.Printf("Starting server on 8080\n")
	if err := http.ListenAndServe(":8080", mux); err != nil {
		log.Fatal(err)
	}
	wg.Done()
}
