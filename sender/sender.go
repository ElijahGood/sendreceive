package sender

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http"
	"strconv"
	"sync"
	"time"

	"gitlab.com/sendrecieve/types"
)

type SenderHandler struct {
	Duration time.Duration
	Buffer   chan int
}

func (sh *SenderHandler) sendRequest(data *types.JsonForm) {
	payloadBuf := new(bytes.Buffer)
	json.NewEncoder(payloadBuf).Encode(data)
	req, _ := http.NewRequest("POST", "http://localhost:8080", payloadBuf)

	client := &http.Client{Timeout: 3 * time.Second}
	res, e := client.Do(req)
	if e != nil {
		log.Fatal(e)
	}

	defer res.Body.Close()

	// log.Printf("sending[%d]: %s;	(%s)\n", data.UserID, data.Message, res.Status)
}

func Generator(number int, ch chan types.JsonForm) {
	for i := 0; i < number; i++ {
		data := types.JsonForm{
			UserID:  i,
			Message: "Hello, " + strconv.Itoa(i) + "!",
		}
		ch <- data
	}
	close(ch)
}

func Sender(di *types.DI, wg *sync.WaitGroup, buffer <-chan types.JsonForm) {
	handler := &SenderHandler{
		Duration: (time.Minute / time.Duration(di.Send)),
		Buffer:   make(chan int, di.Send),
	}
	ticker := time.Tick(handler.Duration)

	for data := range buffer {
		<-ticker
		go func(data *types.JsonForm) {
			handler.sendRequest(data)
		}(&data)
	}
	wg.Done()
}
