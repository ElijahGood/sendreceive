package main

import (
	"sync"
	"testing"

	"gitlab.com/sendrecieve/arguments"
	"gitlab.com/sendrecieve/receiver"
	"gitlab.com/sendrecieve/sender"
	"gitlab.com/sendrecieve/types"
)

func TestArgumetsTooMany(t *testing.T) {
	fake_args := []string{"-c", "42", "-f", "42", "-k"}
	_, _, err := arguments.ArgsHandler(fake_args)
	if err != nil {
		t.Log(err)
	} else {
		t.Errorf("%v IS VALID", fake_args)
	}
}

func TestArgumetsOnlyNumbers(t *testing.T) {
	fake_args := []string{"13", "42", "100", "99"}
	_, _, err := arguments.ArgsHandler(fake_args)
	if err != nil {
		t.Log(err)
	} else {
		t.Errorf("%v IS VALID", fake_args)
	}
}

func TestArgumetsOnlyFlags(t *testing.T) {
	fake_args := []string{"-f", "-x", "-c", "-z"}
	_, _, err := arguments.ArgsHandler(fake_args)
	if err != nil {
		t.Log(err)
	} else {
		t.Errorf("%v IS VALID", fake_args)
	}
}

func TestArgumetsOnlyTwoFlags(t *testing.T) {
	fake_args := []string{"-f", "-c"}
	_, _, err := arguments.ArgsHandler(fake_args)
	if err != nil {
		t.Log(err)
	} else {
		t.Errorf("%v IS VALID", fake_args)
	}
}

func TestOneOne(t *testing.T) {
	di := &types.DI{Send: 1, Receive: 1}
	var wg sync.WaitGroup
	wg.Add(1)
	go receiver.RunReceiver(di, &wg)
	ch := make(chan types.JsonForm)
	go sender.Generator(di.Send, ch)
	go sender.Sender(di, &wg, ch)
	wg.Wait()
}

func TestFtOne(t *testing.T) {
	di := &types.DI{Send: 42, Receive: 1}
	var wg sync.WaitGroup
	wg.Add(1)
	go receiver.RunReceiver(di, &wg)
	ch := make(chan types.JsonForm)
	go sender.Generator(di.Send, ch)
	go sender.Sender(di, &wg, ch)
	wg.Wait()
}

func TestEquals(t *testing.T) {
	di := &types.DI{Send: 20000, Receive: 20000}
	var wg sync.WaitGroup
	wg.Add(1)
	go receiver.RunReceiver(di, &wg)
	ch := make(chan types.JsonForm)
	go sender.Generator(di.Send, ch)
	go sender.Sender(di, &wg, ch)
	wg.Wait()
}
