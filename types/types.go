package types

type DI struct {
	Receive int
	Send    int
}

type JsonForm struct {
	UserID  int    `json:"user_id"`
	Message string `json:"message"`
}
